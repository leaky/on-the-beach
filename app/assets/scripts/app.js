var app = angular.module('app', [])

	.controller('MainController', ['$scope', '$filter', '$http', function ($scope, $filter, $http) {
		
		$scope.holidays = {};

		$http.get('assets/data/holidays.json')
			.success(function (data) {
				$scope.holidays = data;
				$scope.displayOrder('package.price');
			})
			.error(function () {
				console.log('Error GETing Data');
			})

		var orderBy = $filter('orderBy');

		$scope.displayOrder = function (predicate) {
			console.log(predicate);
			$scope.holidays = orderBy($scope.holidays, predicate);
		};

	}])

	.filter('starRating', function () {
		return function (stars, rating) {
			rating = parseInt(rating);
			for (var i=0; i < rating; i++)
				stars.push(i);
			return stars;
		};
	})