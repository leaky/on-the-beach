$(function() {
	$('body').on('click', 'header > button', function () {
		$(this).parent().find('button').removeClass('active');
		$(this).addClass('active');
	});
});

/**
 * Pretty annoying that this file had to be made
 * just to give the top buttons an active class.
 * short on time to do it in angular.
 */